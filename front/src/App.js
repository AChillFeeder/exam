import React, { useEffect, useState } from "react";
import './App.css';
import { HTTPget } from './useFetch';

function App() {

	const [data, setData] = useState({
		loading: false,
		data: false
	})

	const fetchData = async () => {
		setData({ loading: true });
		let data = await HTTPget("/");
		setData({ loading: false, data: data });
		console.log(data);
	};
	
	useEffect(() => {
		fetchData();
	}, []);

	return (
		<div className="App">
			{}
			{!data.loading && data.data && (	
				<div className="">
					<p>Version: {data.data.version}</p>
					<p>Type de données: {data.data.type}</p>
					<p>{Object.keys(data.data.data).map((champion)=>{
						return <p>Champion name: {champion}</p>
					})}</p>
				</div>
			)}
		</div>
	);
}

export default App;
