Pour déployer le projet:
- Plannifier le déploiement - temps et zone géographique si pertinents:
    Dans ce cas, étant donné que les patches ne sont pas publiés partout en même temps, il faudrait soit suivre le système de Riot soit attendre que le patch soit live partout avant de faire notre propre deploy.
- test et validation
- déploiement dans une sandbox, test sur données et charges reéls 
- push sur la production, suivi intensif pendant le premier et deuxième jour, rester prêt à faire un rollback au cas où.