Ce script met en place un job en stage build qui fait partie de la pipeline "back".

Le job n'est executé que si un tag est specifié dans le commit grâce à la partie "rules" et utilisera une image de docker specifique (docker:24.0.6) avec un alias "docker" qui servirait à référencer cette image

Le script est divisé en trois parties:
- Un login avec les variables d'environnement username: $CI_REGISTRY_USER et password: $CI_REGISTRY_PASSWORD 
- docker build -t (ajoute un tag) $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG (variable d'environnement CI_COMMIT_TAG, on est sûre qu'elle existe grâce au Rule mise en place plus tôt) -t $CI_REGISTRY_IMAGE:latest (et un autre tag) . (point pour indiquer que dockerfile est dans le dossier présent)
