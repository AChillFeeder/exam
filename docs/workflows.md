## Stratégie de release:

2 types de releases:
- patch: bugfixes et changement internes
- mineurs: Après chaque patch LoL, pour intégrer du nouveau contenu (nouveau champions, items...)
- majeurs: Chaque saison, changements majeurs sur l'application

## Stratègie de versionning:
Numérotation sémantique: *Version majeure.Version mineure.Patch*

## Normes de nommage des commits et des branches:

### Règles de nommage - Commits:

régles de nommage: *TYPE-NUMERO*
TYPE: Feature par exemple, description du type de changement (FEATURE-BUGFIX-REFACTORING-DOCUMENTATION)
NUMERO: numéro du ticket
Ex: BUGFIX-13

### Règles de nommage - Branche:

Patch LoL-Version Projet
Ex: 13.10-1.5
