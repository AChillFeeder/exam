
# Persona

|Caractéristique du Persona|Persona 1 - Compétitif|Persona 2 - Jeu Occasionnel|
|----|----|----|
|Contexte|Etudiant universitaire et joueur passionné|Professionnel avec travail en temps plein, apprécie le jeu vidéo comme passe-temps occasionnel|
|Caractéristiques|Joueur Passionné, Esprit Compétitif, Apte à la Technologie, Soucieux du Temps|Joueur Occasionnel, Temps Limité
|Utilisation de l'Assistant|Guides de Build, Analyse de Partie, Stratégies Contre, Alertes Chronométrées|Sélection de Champions, Conseils de Stratégies générales, Mises à Jour d'Actualités

# Contextes et Stories

- Préparation des matchs classés
    - En tant que Persona 1, je veux pouvoir obtenir des informations sur les champions présents et leurs sorts dans la partie afin de pouvoir mieux coordonner avec mes coéquipiers et à mieux punir les fautes de l'opposition
    - En tant que Persona 2, je souhaite obtenir des informations générales et des guides simples sur mon caractère avant le jeu afin d'éviter d'apprendre par cœur mes compétences.
    - En tant que Persona 1, je veux pouvoir personnaliser ma Dashboard pour n'afficher que les informations nécessaires afin de pouvoir consulter les données en plein jeu sans me perdre
- Suivi des actualités
    - En tant que persona 1, je souhaite rester à jour sur tous les changements, bug fixes, eSport, dev blogs etc. depuis mon interface afin de rester à jour et continuer de développer mes compétences
    - En tant que persona 2, je souhaite qu'on me présente les changements que le jeu à subi pendant mon absence afin d'éviter de lire plusieurs patches notes chaque fois que je reviens.
    - En tant que persona 2, je souhaite rester au courant au sujet des nouveautés sur le jeu (rotating game mode, cinématiques...) afin de me décider de revenir au jeu ou pas selon mes préférences.




