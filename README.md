
# Introduction

"Supp diff" est un projet destiné à supporter les joueurs pendant une partie LoL en présentant des informations vitales d'une façon claire et concise.

C'est un projet qui se veut :
    - Minimaliste : éviter des features gourmandes en mémoire ou débit
    - Clair : prioritiser la clarté des informations présentés à la quantité, garder le minimum possible de données
    - Personnalisable : L'utilisateur doit pouvoir personnaliser certains aspects de l'application selon ses besoins

# Fonctionnement
En phase finale, le projet doit pouvoir fonctionner de la façon suivante
- L'utilisateur accède à son Dashboard et configure son application (nom du compte LoL et paramétrage/personnalisation)
- L'utilisateur lance son jeu, 'Supp diff' détecte automatiquement quand il serait en partie
- L'application affichera les données demandées

# Pour aller plus loin :
- Authentification
- Personnalisation poussée (données différentes selon le type de match ou des champions présents, choix du match up en cours...)
- Enregistrement des données



