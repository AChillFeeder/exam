import pytest
import requests

def city_endpoint():
    return requests.get('http://0.0.0.0:5000/')
    
def test_city_endpoint_status_code():
    assert city_endpoint().status_code == 200
