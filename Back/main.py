# N OUBLIE PAS DE RENNOMER back à Back DANS LE FICHIER POUR LA QUESTION LIVRAISON CONTINUE

from flask import Flask
import requests, json
from flask_cors import CORS

app = Flask(__name__)

CORS(app)

@app.route('/')
def index():
    data = requests.get('http://ddragon.leagueoflegends.com/cdn/13.22.1/data/en_US/champion.json').json()
    return data

# IMPORTANT, GARDER LE HOST EN TANT QUE 0.0.0.0
if __name__ == "__main__":
    app.run('0.0.0.0', 5000, True)
 

# DEUXIEME RAPPEL